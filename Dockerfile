FROM node:boron

# Create app directory
RUN mkdir -p /usr/src/nautisettings
WORKDIR /usr/src/nautisettings

# Install dependencies
COPY package.json /usr/src/nautisettings/
RUN npm install

# Bundle source
COPY . /usr/src/nautisettings

# Expose app
EXPOSE 3000

# Run app
CMD [ "npm", "start" ]