var express = require('express');
var app = express();

var bodyParser = require('body-parser');

var SettingsDb = require('./db/settings');
var Setting = SettingsDb; // SettingsDb object overloaded as constructor; split it up

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}));

// get all settings
app.get('/api/settings', function(req, res) {
    SettingsDb.find(function(err, results) {
        if (err) { 
            res.status(500).send(err); 
        }
        else if (!results.length) {
            res.status(404).json({message: 'Settings collection is empty'});
        }
        else {
            res.json(results);
        }
    });
});

// get setting by name
app.get('/api/settings/:key', function(req, res) {
    SettingsDb.findOne({key: req.params.key}, function(err, result) {
        if (err) { 
            res.status(500).send(err); 
        }
        else if (!result) {
            res.status(404).json({message: 'Key not found'});
        }
        else {
            res.json(result);
        }
    });
});

// create new setting
app.post('/api/settings', function(req, res) {
    var newSetting = new Setting({
        key: req.body.key,
        value: req.body.value
    });

    SettingsDb.create(newSetting, function(err, setting) {
        if (err) {
            if (err.code === 11000) {
                res.status(409).json({message: "Setting with that key already exists"})
            }
            else {
                res.status(500).send(err);
            }
        }
        else {
            res.json({action: 'created', setting: setting});
        }
    });
});

// update setting by name
app.put('/api/settings/:key', function(req, res) {
    SettingsDb.findOneAndUpdate({key: req.params.key}, {key: req.body.key, value: req.body.value}, function(err, setting) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            res.json({action: 'updated', setting: setting});
        }
    });
});

// delete setting by name
app.delete('/api/settings/:key', function(req, res) {
    SettingsDb.findOneAndRemove(req.params.key, function(err, setting) {
        if (err) {
            res.send(500, err);
        }
        else {
            res.json({action: 'deleted', setting: setting});
        }
    });
});

// catch-all
app.get('*', function(req, res) {
    res.send('NautiSettings\n');
    // todo: send a page that lists out the api members
});

var port = process.env.PORT || 3000;
app.listen(port, function() {
    console.log('listening on ' + port);
});