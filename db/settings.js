var mongoose = require('mongoose');

var dbUser = "dev";
var dbPass = "friskydingo";
mongoose.connect('mongodb://'+dbUser+':'+dbPass+'@ds133368.mlab.com:33368/ejh891_devdb');

var settingsSchema = mongoose.Schema({
    key : { type: String, unique: true, required: true },
    value : { type: mongoose.Schema.Types.Mixed, required: true }
});

var Settings = mongoose.model('settings', settingsSchema);

module.exports = Settings;